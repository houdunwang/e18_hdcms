export default params => [
    { title: '公众号列表', name: 'site.wechat.index', params },
    { title: '添加公众号', name: 'site.wechat.create', params },
    { title: '编辑公众号', name: 'site.site.edit', params, current: true }
]
