export default [
    { label: '套餐列表', name: 'index', route: 'Edu.admin.subscribe.index' },
    { label: '添加套餐', name: 'create', route: 'Edu.admin.subscribe.create' },
    { label: '修改套餐', name: 'edit', route: 'Edu.admin.subscribe.edit', current: true }
]
