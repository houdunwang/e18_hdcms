export default [
    { label: '课程列表', name: 'index', route: 'Edu.admin.lesson.index' },
    { label: '添加课程', name: 'create', route: 'Edu.admin.lesson.create' },
    { label: '修改课程', name: 'edit', route: 'Edu.admin.lesson.edit', current: true }
]
