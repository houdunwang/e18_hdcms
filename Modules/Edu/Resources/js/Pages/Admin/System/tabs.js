export default [
    { label: '课程列表', name: 'index', route: 'Edu.admin.system.index' },
    { label: '添加课程', name: 'create', route: 'Edu.admin.system.create' },
    { label: '修改课程', name: 'edit', route: 'Edu.admin.system.edit', current: true }
]
