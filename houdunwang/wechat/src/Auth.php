<?php

namespace Houdunwang\WeChat;

use Houdunwang\WeChat\Auth\Authorize;

class Auth extends WeChat
{
  use Authorize;
}
